from flask import Flask, Response, request
from flask_cors import CORS
import json

app = Flask(__name__)
CORS(app)

todo_items = [
    {
        "id": 1, "title": "Study Japanese", "description": "Study for the JLPT N4 exam",
        "is_completed": False
    },
    {
        "id": 2, "title": "Grocery Shopping", "description": "Buy bread, eggs, and milk.",
        "is_completed": False
    },
    {
        "id": 3, "title": "Book Flight", "description": "Book a flight to Honolulu.",
        "is_completed": True
    },
]


@app.route('/items', methods=['GET', 'POST', 'DELETE', 'PUT'])
def items():
    if request.method == 'POST':
        request.json["id"] = todo_items[-1]["id"] + 1
        todo_items.append(request.json)
    elif request.method == 'DELETE':
        for idx, current_item in enumerate(todo_items):
            if current_item["id"] == request.json["id"]:
                todo_items.pop(idx)
                break
    elif request.method == 'PUT':
        for current_item in todo_items:
            if current_item["id"] == request.json["id"]:
                current_item["title"] = request.json["title"]
                current_item["description"] = request.json["description"]
                current_item["is_completed"] = request.json["is_completed"]
                break
    return Response(json.dumps(todo_items), mimetype='application/json')


@app.route('/item/<item_id>', methods=['GET'])
def item(item_id):
    for current_item in todo_items:
        if current_item["id"] == int(item_id):
            return Response(json.dumps(current_item), mimetype='application/json')


if __name__ == '__main__':
    app.run()
